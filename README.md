# Manjaro WSL with Raylib Sample Project

This repository aims to explore the possibility of running an window application with graphics using raylib on Manjaro's Windows Subsystem for Linux (WSL).

## Project Overview

This sample project, provides a simple 3D program that utilizes raylib, a popular and easy-to-use game development library. The goal is to test the compatibility and feasibility of running an 3D application with raylib on the Manjaro Linux distribution running on WSL.

The latest tested version works with:

- **ManjaroWSL2:** ManjaroWSL2-20230601
- **WSL:** Version 1.2.5.0

## Prerequisites

Before getting started, please ensure you have the following:

- [ManjaroWSL2](https://github.com/sileshn/ManjaroWSL2)
- Git installed on your system to clone this repository
- WSL 2 with support for Linux GUI apps (you can follow [this](https://learn.microsoft.com/en-us/windows/wsl/tutorials/gui-apps) docs)

Once you have all the prerequistes installed open your wsl terminal.

### Getting raylib

Raylib can be easily installed via the Arch User Repository:

```bash
yay -S raylib
```

### Setup Instructions
  
To get started with this sample project, follow these steps:

Clone this repository to your local machine:

```bash
git clone https://gitlab.com/Fleischpostman/raylib-manjaro-wsl-test.git
```

Navigate to the project directory:

```bash
cd raylib-manjaro-wsl-test
```

Build the project:

```bash
./build.sh
```

Run the program:

```bash
./build/demo
```

## Contributing

If you encounter any issues while running the demo or have suggestions for improvements, feel free to contribute! Please feel free to open issues or submit pull requests to help improving this sample project.

## License

This sample project is licensed under the MIT License. For more details, please refer to the LICENSE file.
