#!/bin/bash

# NOTE(Fabi): To have a clean build we always delete the complete build directory
#             before we compile the application.
rm -rf build
mkdir build

gcc demo/main.cpp -o ./build/demo -lraylib -lGL -lm -lpthread -ldl -lrt -lX11