/*******************************************************************************************
*
* This demo is an altered version of: raylib [core] example - 3d camera first person
* Check it out on https://www.raylib.com/examples.html
*
* Original license:   
*
* Example originally created with raylib 1.3, last time updated with raylib 1.3
*
* Example licensed under an unmodified zlib/libpng license, which is an OSI-certified,
* BSD-like license that allows static linking with closed source software
*
* Copyright (c) 2015-2023 Ramon Santamaria (@raysan5)
*
********************************************************************************************/

#include "raylib.h"

const int MaxColumns = 20;

int main()
{
    // NOTE(Fabi): Creating the window
    {
        const int screenWidth = 800;
        const int screenHeight = 450;

        InitWindow(screenWidth, screenHeight, "ManjaroWSL2 Demo App");
        SetWindowPosition(200, 100);

        SetTargetFPS(60);  
    }

    // NOTE(Fabi): Setting up the scene 
    Camera camera = {};
    int cameraMode = CAMERA_FIRST_PERSON;
    float heights[MaxColumns] = {};
    Vector3 positions[MaxColumns] = {};
    Color colors[MaxColumns] = {};
    {
        camera.position = Vector3{0.0f, 2.0f, 4.0f};
        camera.target = Vector3{0.0f, 2.0f, 0.0f};
        camera.up = Vector3{0.0f, 1.0f, 0.0f};    
        camera.fovy = 60.0f;
        camera.projection = CAMERA_PERSPECTIVE;

        // Generates some random columns
        for(int i = 0; i < MaxColumns; i++)
        {
            heights[i] = (float)GetRandomValue(1, 12);
            positions[i] = Vector3{(float)GetRandomValue(-15, 15), heights[i] / 2.0f, (float)GetRandomValue(-15, 15)};
            colors[i] = Color{(unsigned char)GetRandomValue(20, 255), (unsigned char)GetRandomValue(10, 55), 30, 255};
        }
    }

    while(!WindowShouldClose())
    {
        UpdateCamera(&camera, cameraMode);

        BeginDrawing();
        ClearBackground(RAYWHITE);
            
        // NOTE(Fabi): Draw 3D scene
        {
            BeginMode3D(camera);

            DrawPlane(Vector3{0.0f, 0.0f, 0.0f}, Vector2{32.0f, 32.0f}, LIGHTGRAY); // Draw ground
            DrawCube(Vector3{-16.0f, 2.5f, 0.0f}, 1.0f, 5.0f, 32.0f, BLUE);           // Draw a blue wall
            DrawCube(Vector3{16.0f, 2.5f, 0.0f}, 1.0f, 5.0f, 32.0f, LIME);            // Draw a green wall
            DrawCube(Vector3{0.0f, 2.5f, 16.0f}, 32.0f, 5.0f, 1.0f, GOLD);            // Draw a yellow wall
            for (int i = 0; i < MaxColumns; i++)
            {
                DrawCube(positions[i], 2.0f, heights[i], 2.0f, colors[i]);
                DrawCubeWires(positions[i], 2.0f, heights[i], 2.0f, MAROON);
            }
            EndMode3D();
        }

        // NOTE(Fabi): Drawing controls
        {
            DrawRectangle(5, 5, 330, 100, Fade(SKYBLUE, 0.5f));
            DrawRectangleLines(5, 5, 330, 100, BLUE);

            DrawText("Camera controls:", 15, 15, 10, BLACK);
            DrawText("- Move keys: W, A, S, D, Space, Left-Ctrl", 15, 30, 10, BLACK);
            DrawText("- Look around: arrow keys or mouse", 15, 45, 10, BLACK);
        }

        EndDrawing();
    }

    CloseWindow();

    return 0;
}